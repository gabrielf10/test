from sys import argv
import json

'''
1. Recibo informacion personal en multiples formatos
2. La convierto a formato JSON
3. Muestro la informacion formateada con 2 espacios de indentacion y las llaves ordenadas alfabeticamente
4. El Programa recibe un archivo de n lineas
5. Cada linea contiene informacion: first name, last name, phone number, color, zip code.
6. El programa debe poder leer en cualquier de las 3 formas de ingreso de datos
* Apellido, Nombre, telefono, color, codigo
* Nombre y Apellido, color, codigo, telefono
* Nombre, Apellido, codigo, telefono, color
7. Debo invalidar las lineas que contengan un numero incorrecto de digitos de numero de telefono pero el sistema debe continuar analisando las subsiguientes
'''

class Test(object):

	#Constructor
	def __init__(self, file=None, validate_line=None):
		self.file = file
		self.open_file()
		self.get_line()

	# Abro el archivo y guardo las filas en un array llamado lines
	def open_file(self):
		file = open(self.file, "r")
		self.lines = file.readlines()
		print self.lines
		return self.lines

	# Recorro el array lines Obtengo el indice en la variable index y cada una de las lineas en formato string en la variable line
	# El indice lo voy a usar mas adelante para mostrar que linea dio error.
	def get_line(self):
		validate_line = []	   
		for index, line in enumerate(self.lines):
			print index
			print line
			self.validate(line, index)

	def validate(self, line, index):
        #Llamar a las validaciones y guardarlas en un array si todas son true entonces guardaria los datos en un diccionario con append sino significa que da error
        # y guardaria el indice en un array de aca voy a llamar al metodo que va a generar el JSON a partir de estas listas 
		return line
	    	



if __name__ == '__main__':
    if len(argv) == 2:
        file = argv[1]
        test = Test(file)       
    else:
        raise Exception("Debe pasar un archivo de texto")



